export const homeData = [
    {
        heading: 'The Motherless Child Foundation',
        description:
            'A 501(c)(3) non-profit public charity, was founded by two parents who fell in love with all of the children in an orphanage when they went to adopt only two. Three adoptions and four children later, we are still reaching out to those children we had to leave behind. Another adoptive parent explained it best, I went home and cried for the children I left behind. [They] went home and started a charity.',
    },
    {
        heading: 'A proven track record',
        description:
            'We have been helping orphans since 2003 when we traveled to Kazakhstan to adopt for the first time. Since then, our programs have grown every year. We now travel twice a year to three orphanages to provide support to all of the children there.',
    },
    {
        heading: 'Will you please help us help orphans?',
        description:
            'With all of the children in three orphanages depending on us, we are no longer able to do it alone. We need your help to operate our programs. One hundred percent of your donation will support programs benefiting children. Our all volunteer board, officers and staff take no salaries. We have no overhead. Our founders donate all administrative expenses. Volunteers make the trips to Kazakhstan.',
    },
    {
        heading: 'The children are counting on us.',
        description:
            'A recently adopted seven year old boy told his new mama that the children always wait with great anticipation for our visits. We don\'t want to disappoint them.  Learn more about our programs in the orphanages.',
    },
]