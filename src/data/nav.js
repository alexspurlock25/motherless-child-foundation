export const navData = [
    {
        id: 1,
        name: "Home",
        route: "/"
    },
    {
        id: 2,
        name: "Programs",
        route: "/programs"
    },
    {
        id: 3,
        name: "How to Help",
        route: "/how-to-help"
    },
    {
        id: 4,
        name: "Fundraisers",
        route: "/fundraisers"
    }
]