import React, { Fragment } from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';


import Home from './Home';
import Programs from "./Programs";
import HowToHelp from "./HowToHelp";
import Fundraisers from "./Fundraisers";

import { navData } from '../data/nav';

function Nav() {
    return (
        <Fragment>
            <BrowserRouter>
                <nav className="">
                    <ul className="text-center">
                        {
                            navData.map((page) => {
                                return (
                                    <li key={ page.id } className="inline-block mx-2">
                                        <Link
                                            className="font-medium py-4 px-8 rounded-md shadow-lg hover:shadow-md hover:text-blue-400 transition duration-300"
                                            to={ page.route }
                                        >
                                            { page.name }
                                        </Link>
                                    </li>
                                );
                            })
                        }
                    </ul>
                </nav>

                <Route exact path="/" component={Home} />
                <Route exact path="/programs" component={Programs} />
                <Route exact path="/how-to-help" component={HowToHelp} />
                <Route exact path="/fundraisers" component={Fundraisers} />
            </BrowserRouter>
        </Fragment>
    );

}


export default Nav;

