import React from "react";

import { buttonStyles } from '../data/styles'

function ContactUsFooter() {
    return (
        <React.Fragment>
            <p className="mb-1">Motherless Child Foundation, Inc.<br />735 Monmouth St.<br />Newport, KY 41071</p>
            <div className="flex flex-wrap content-center justify-center">
                <a className={ "flex flex-wrap content-center justify-center " + buttonStyles } href="mailto:motherlesschildfoundation@aol.com">Email Us!</a>
            </div>
        </React.Fragment>
    );
}

export default ContactUsFooter;