import React from 'react';

import banner from '../static/images/banner.jpg';

function Banner() {
    return (
        <div className="bg-blue-400 py-5 mb-10 w-100">
            <img className="w-50 mx-auto rounded-lg shadow-lg" src={banner} alt="Motherless Child Foundation logo." />
        </div>
    );
}

export default Banner;