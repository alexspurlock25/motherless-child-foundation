import React from 'react';

function PageTitle({ title }) {
    return (
        <h1 className="mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">{ title }</h1>
    );
}

export default PageTitle;