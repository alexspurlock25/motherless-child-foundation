import React from 'react';

import Banner from './Banner';
import Nav from './Nav';
import Footer from "./Footer";

function App() {
  return (
      <React.Fragment>
          <Banner />
          <div className="container mx-auto">
              <Nav />
          </div>
          <Footer />
      </React.Fragment>
  );
}

export default App;
