import React from 'react';

import PageTitle from "./PageTitle";

function Programs() {
    return (
        <React.Fragment>
            <PageTitle title="Our Programs" />
            <div className="content-text">
                These are our programs
            </div>
        </React.Fragment>
    );
}

export default Programs;