import React from 'react';

import PageTitle from "./PageTitle";

function Fundraisers() {
    return (
        <React.Fragment>
            <PageTitle title="Fundraisers" />
            <div className="content-text">
                Fundraisers are here!
            </div>
        </React.Fragment>
    );
}

export default Fundraisers;