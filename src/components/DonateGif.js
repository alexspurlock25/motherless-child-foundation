import React from 'react';

import { buttonStyles } from '../data/styles';

function DonateGif() {
    return (
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank" className="">
            <input type="hidden" name="cmd" value="_s-xclick"/>
            <input type="hidden" name="hosted_button_id" value="6U5ULXDWFFTBC"/>
            <input className={ buttonStyles } type="submit" name="submit" value="Donate!"/>
        </form>
    );
}

export default DonateGif;
