import React from 'react';

import PageTitle from "./PageTitle";

import { homeData } from '../data/home';

function Home() {
    return (
        <div className="py-12 bg-white">
            <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <div className="lg:text-center">
                    <PageTitle title="Our Story" />
                </div>
                <div className="mt-10">
                    <dl className="space-y-10 md:space-y-0 md:grid md:grid-cols-2 md:gap-x-8 md:gap-y-10">
                        {
                            homeData.map((feature) => (
                                <div key={feature.heading} className="relative">
                                    <dt>
                                        <p className="ml-16 text-lg leading-6 font-medium text-gray-900">{feature.heading}</p>
                                    </dt>
                                    <dd className="mt-2 ml-16 text-base text-gray-500">{feature.description}</dd>
                                </div>
                            ))
                        }
                    </dl>
                </div>
            </div>
        </div>
    );
}

export default Home;

