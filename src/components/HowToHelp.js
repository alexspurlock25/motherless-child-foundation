import React from 'react';

import PageTitle from "./PageTitle";

function HowToHelp() {
    return (
        <React.Fragment>
            <PageTitle title="How To Help" />
            <div className="content-text">
                This is how you can help!
            </div>
        </React.Fragment>
    );
}

export default HowToHelp;