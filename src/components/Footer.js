import React from "react";

import DonateGif from "./DonateGif";
import SubscribeForm from "./SubscribeForm";
import ContactUsFooter from "./ContactUsFooter";

function Footer() {
    return (
        <footer>
            <div className="flex bg-blue-400 py-4">
                <div className="flex-1 text-center place-self-center">
                    <DonateGif />
                </div>
                <div className="flex-1 text-center">
                    <SubscribeForm />
                </div>
                <div className="text-center flex-1">
                    <ContactUsFooter />
                </div>
            </div>
            <small className="bg-blue-500 h-10 flex flex-wrap content-center justify-center shadow-inner">Copyright 2011 Motherless Child Foundation, Inc.  All rights reserved.</small>
        </footer>
    );
}

export default Footer;