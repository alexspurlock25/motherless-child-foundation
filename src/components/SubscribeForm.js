import React from 'react';

import { buttonStyles } from '../data/styles';

function SubscribeForm() {
    return (
        <React.Fragment>
            <h3 className="text-lg font-semibold text-center">Subscribe to our mailing list</h3>
            <form
                action="http://motherlesschildfoundation.us2.list-manage.com/subscribe/post?u=feef3b984abb70273c6f96d6f&amp;id=736452c2f2"
                method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form"
                className=""
                target="_blank">
                <input type="email" value="" name="email" className="w-60 h-9 pl-2 mb-1 rounded-md focus:ring-blue-400" id="mce-EMAIL" placeholder="Email Address" required=""/>
                <input type="submit" value="Subscribe" name="subscribe"
                                              id="mc-embedded-subscribe" className={ buttonStyles } />
            </form>
        </React.Fragment>
    );
}

export default SubscribeForm;